<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('action/editer_rubrique');

/**
 * Chargement des donnees du formulaire
 *
 * @param int $id_rubrique
 *
 * @return array
 */
function formulaires_editer_tri_rubrique_charger($id_rubrique, $redirect) {

	$valeurs = [];

	// On passe au formulaire l'id de la rubrique.
	$valeurs['id_rubrique'] = $id_rubrique;

	// un choix est déjà sélectionné ou pas ?
	if ($infos_tri = sql_fetsel(['trirub_articles', 'trirub_articles_inverse'], 'spip_rubriques', 'id_rubrique=' . intval($id_rubrique))) {
		$valeurs = array_merge($valeurs, $infos_tri);
	}

	// On détermine si on est autorisé
	$valeurs['editable'] = autoriser('modifier', 'rubrique', $id_rubrique);

	return $valeurs;
}

/**
 * Traitement
 *
 * @param int $id_rubrique
 *
 * @return array
 */
function formulaires_editer_tri_rubrique_traiter($id_rubrique, $redirect) {

	$retour = [
		'message_ok' => '',
		'editable'   => true,
	];

	if (!_request('annuler')) {
		$update = [];
		$tri = _request('trirub_articles');
		$sens = _request('trirub_articles_inverse');
		// si on a choisi la config par défaut, on efface les valeurs pour la rubrique
		if ($tri == 'defaut') {
			$tri = '';
			$sens = '';
		}
		if (
			!is_null($tri)
			and !is_null($sens)
		) {
			$update['trirub_articles'] = $tri;
			$update['trirub_articles_inverse'] = $sens;
			rubrique_modifier($id_rubrique, $update);
		}

		if ($redirect) {
			$retour['redirect'] = $redirect;
		}
	}

	return $retour;
}
