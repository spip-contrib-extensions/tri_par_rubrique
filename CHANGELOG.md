# Changelog

## [1.7.2] - 2025-01-29

- #11 : sans id_rubrique dans le contexte, utiliser le sens par défaut

## [1.7.1] - 2025-01-05

- #10 : la bone valeur de config à l'installation

## [1.7.0] - 2024-11-13

- La config par défaut pour les rubriques à l'installation passe à `num titre`
- #8 Prendre en compte le nouveau tri `{par num titre}` de SPIP 4.x

## [1.6.1] - 2024-10-15

### Fixed

- le `tri` configuré doit être disponible pour le formulaire d'édition même si on à pas l’autorisation de modifier la
  rubrique car sinon ce n'est pas la bonne valeur qui s'affiche !

## [1.5.0] - 2023-04-16

### Changed

- Compatibilité SPIP 4.2+

### Fixed

- Reprise du critère {id_?} dans la surcharge de prive/objets/liste/articles.html

## [1.4.9] - 2022-12-06

### Fixed

- Bug sur la recherche d'id_rubrique dans le contexte, on oubliait la première boucle

## [1.4.8] - 2022-11-28

### Fixed

- Retrouver l'id_rubrique selon le contexte

## [1.4.7] - 2022-05-25

### Added

- Compatibilité SPIP 4.1

### Fixed

- Adaptation au thème CSS de SPIP 4
- Suppression de l'attribut inutile enctype du formulaire de configuration du tri de la rubrique
- Ne générer l'attribut data-objets sur la liste d'articles que si la rubrique a un tri par rang
