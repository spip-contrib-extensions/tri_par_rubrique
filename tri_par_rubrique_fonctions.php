<?php
/**
 * Fonctions utiles au plugin Tri des articles par rubrique
 *
 * @plugin     Tri des articles par rubrique
 * @copyright  2019
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Tri_par_rubrique\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Génère la liste des critères de tri d'article utilisables sur les rubriques
 *
 * @return array
 */
function filtre_tri_par_rubrique_criteres_dist($id_rubrique) {
	$criteres = [
		'date'       => _T('tri_par_rubrique:tri_articles_date'),
		'maj'        => _T('tri_par_rubrique:tri_articles_maj'),
		'titre'      => _T('tri_par_rubrique:tri_articles_titre'),
		'num titre'  => _T('tri_par_rubrique:tri_articles_num_titre'),
		'id_article' => _T('tri_par_rubrique:tri_articles_id_article'),
	];

	// pour les rubriques, proposer le choix "Configuration par défaut"
	$config_defaut = lire_config('tri_par_rubrique', []);
	if (isset($config_defaut['trirub_articles']) && $id_rubrique) {
		$label = _T('tri_par_rubrique:tri_articles_defaut')
			. ' : ' . _T('tri_par_rubrique:tri_articles_' . str_replace(' ', '_', $config_defaut['trirub_articles']))
			. ($config_defaut['trirub_articles_inverse'] ? ' - ' . _T('tri_par_rubrique:tri_articles_inverse') : '');
		$criteres = array_merge(['defaut' => $label], $criteres);
	}

	if (
		test_plugin_actif('rang')
		and function_exists('rang_liste_objets')
		and in_array('articles', rang_liste_objets())
	) {
		$criteres['rang'] = _T('tri_par_rubrique:tri_articles_rang');
	}

	foreach ($criteres as $key => $value) {
		$criteres[$key] = _T('tri_par_rubrique:tri_articles_par') . ' ' . $value;
	}

	return $criteres;
}

/**
 * Critère tri_rubrique, qui affiche les articles selon le tri défini sur la rubrique dans l'espace privé
 *
 * @param $idb
 * @param $boucles
 * @param $crit
 *
 * @return string[]
 */
function critere_tri_rubrique_dist($idb, &$boucles, $crit) {

	$boucle = &$boucles[$idb];

	// Uniquement les boucles articles sont concernées par le critère de tri.
	if ($boucle->id_table != 'articles') {
		return ['zbug_tri_rubrique_sur_articles_uniquement'];
	}

	// nécessaires pour le tri par "num titre"
	$boucle->select[] = '0+articles.titre AS num';
	$boucle->select[] = 'CASE ( 0+articles.titre ) WHEN 0 THEN 1 ELSE 0 END AS sinum';

	// Détermination du tri configuré pour la rubrique ou globalement par défaut si aucun id_rubrique dans l'environnement
	$boucle->order[] = 'calculer_tri_rubrique($Pile, ($collecte ?? ""))';
}

function calculer_tri_rubrique($Pile, $collecte = '') {
	// chercher l'id_rubrique dans le contexte
	do {
		$depile = array_pop($Pile);
		$id_rubrique = $depile['id_rubrique'] ?? null;
	} while (!$id_rubrique && !empty($Pile));

	// On construit l'expression de tri.
	if ($tri = tri_rubrique_champ(intval($id_rubrique))) {
		// cas spécial "par num titre"
		if ($tri == 'num titre') {
			$tri = 'sinum, num';
		}
		$sens = tri_rubrique_sens($id_rubrique) ? 'DESC' : 'ASC';

		return "$tri $collecte $sens";
	}

	return '';
}

function tri_rubrique_champ($id_rubrique) {
	static $cache;
	if (isset($cache[$id_rubrique])) {
		return $cache[$id_rubrique];
	}

	// On initialise le champ à vide.
	$champ = '';

	if ($id = intval($id_rubrique)) {
		// On a bien un id_rubrique dans l'environnement
		$champ = sql_getfetsel('trirub_articles', 'spip_rubriques', 'id_rubrique=' . $id);
	}

	if (!$champ) {
		// Pas d'id_rubrique ou pas de valeur pour la rubrique, on se rabat sur la config globale
		include_spip('inc/config');
		$champ = lire_config('tri_par_rubrique/trirub_articles');
	}

	$cache[$id_rubrique] = $champ;

	return $champ;
}

function tri_rubrique_sens($id_rubrique) {
	static $cache;
	if (isset($cache[$id_rubrique])) {
		return $cache[$id_rubrique];
	}

	// On initialise la variable à false pour la distinguer de 0.
	$inverse = false;

	if ($id = intval($id_rubrique)) {
		if (sql_getfetsel('trirub_articles', 'spip_rubriques', 'id_rubrique=' . $id)) {
			$inverse = sql_getfetsel('trirub_articles_inverse', 'spip_rubriques', 'id_rubrique=' . $id);
		}
	} else {
		// Pas d'id_rubrique, on se rabat sur la config globale
		include_spip('inc/config');
		$inverse = lire_config('tri_par_rubrique/trirub_articles_inverse');
	}

	$cache[$id_rubrique] = $inverse;

	return $inverse;
}
